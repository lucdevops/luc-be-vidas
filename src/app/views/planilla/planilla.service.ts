import { Rutas } from './../../rutas';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Marca } from '../../model/marca';
import { Observable } from 'rxjs';
import { Cliente } from '../../model/clientes';

@Injectable({
  providedIn: 'root'
})
export class PlanillaService {

  constructor(private http: HttpClient, private r: Rutas) { }

  cargarMar(): Observable<Marca[]> {
    return this.http.get<Marca[]>(this.r['datosWMS'] + '/cargaMar');
  }

  cargaCli(descripcion): Observable<Cliente[]> {
    return this.http.get<Cliente[]>(this.r['datosWMS'] + '/cargaCli/' + descripcion);
  }

  cargarCliDia(dia): Observable<Cliente[]> {
    console.log(this.r['datosWMS'] + '/cargaCliDia/' + dia);
    return this.http.get<Cliente[]>(this.r['datosWMS'] + '/cargaCliDia/' + dia);
  }

  crearPedidoWMS(proveedor, agendados) {
    return this.http.get(this.r['crearWMS'] + '/crearPedidoWMS/' + agendados + '/' + proveedor);
  }


}
