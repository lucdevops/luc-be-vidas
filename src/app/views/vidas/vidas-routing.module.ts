import { PresupuestoComponent } from './presupuesto/presupuesto.component';
import { RutasComponent } from './rutas/rutas.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Vidas'
    },
    children: [
      /*{
        path: '',
        redirectTo: 'agendados'
      },*/
      {
        path: 'rutas',
        component: RutasComponent,
        data: {
          title: 'Rutas'
        }
      },
      {
        path: 'presupuesto',
        component: PresupuestoComponent,
        data: {
          title: 'Presupuesto'
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VidasRoutingModule {}
