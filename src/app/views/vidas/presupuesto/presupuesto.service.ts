import { Rutas } from './../../../rutas';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Cliente } from '../../../model/clientes';
import { DatePipe } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class PresupuestoService {

constructor(private http: HttpClient, private r: Rutas, private dp: DatePipe) { }

  cargarVidas(): Observable<Cliente[]> {
    return this.http.get<Cliente[]>(this.r['datosWMS'] + '/cargarVidas');
  }

  cargarPresupuestoProveedor(vendedor: number, mes: number): Observable<any[]> {
    // tslint:disable-next-line: max-line-length
    return this.http.get<any[]>(this.r['datosWMS'] + '/cargarPresupuestoProveedor/' + vendedor + '/' + this.dp.transform(mes, 'yyyy-MM').toString());
  }

  cargarPresupuestoCliente(vendedor: number, mes: number): Observable<any[]> {
    // tslint:disable-next-line: max-line-length
    return this.http.get<any[]>(this.r['datosWMS'] + '/cargarPresupuestoCliente/' + vendedor + '/' +  this.dp.transform(mes, 'yyyy-MM').toString());
  }

  /*cargarDetalleCliente(vendedor: number, mes: number, nit: number): Observable<any[]> {
    return this.http.get<any[]>(this.r['datosWMS'] + '/cargarDetalleCliente/' + vendedor + '/' + mes + '/' + nit);
  }*/
  
  cargarCantidadFacDev(numero: number, texto: string, fechaI: number, fechaF: number): Observable<any[]> {
    return this.http.get<any[]>(this.r['datosWMS'] + '/cargarCantidadFacDev/' + numero + '/' + texto + '/' + fechaI + '/' + fechaF);
  }

}
