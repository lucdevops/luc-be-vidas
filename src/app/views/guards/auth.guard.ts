import { Injectable } from '@angular/core';
import { CanActivate ,Router,ActivatedRouteSnapshot} from '@angular/router';
import { Observable } from 'rxjs';
import { LogginService } from '../login/loggin.service';
import { IfStmt } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private LogginService:LogginService,public router:Router ){}
  canActivate(router: ActivatedRouteSnapshot){
    const expectedRole = router.data.expectedRole;
    const rol=sessionStorage.getItem("cargo");
    var n=false;
    if(expectedRole==='ALL')
    {
      n=true
    }
    else{
      n = expectedRole.includes(rol);
    }
    
    if(this.LogginService.getuser()&&n)
    {          
      return true
    }
    else if(this.LogginService.getuser()&&!n)
    {
      alert("No tienes permiso de ingresar a esta pagina");
  
      this.router.navigate(['/home'])
      return true

    }
    else{
      this.router.navigate(['/loggin'])
      return false
    }
  }
  

}
