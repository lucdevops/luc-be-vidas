import { DetalleComponent } from './detalle/detalle.component';
import { RequerimientosComponent } from './requerimientos/requerimientos.component';
import { ConfiguracionComponent } from './configuracion/configuracion.component';
import { EstimadoComponent } from './estimado/estimado.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Abastecimiento'
    },
    children: [
      /*{
        path: '',
        redirectTo: 'agendados'
      },*/
      {
        path: 'estimado',
        component: EstimadoComponent,
        data: {
          title: 'Estimado de Ventas'
        }
      },
      {
        path: 'configuracion',
        component: ConfiguracionComponent,
        data: {
          title: 'Configuracion'
        }
      },
      {
        path: 'requerimientos',
        component: RequerimientosComponent,
        data: {
          title: 'Orden de Compra'
        }
      },
      {
        path: 'detalle/:numero/:tipo',
        component: DetalleComponent,
        data: {
          title: 'Det Orden de Compra'
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AbastecimientoRoutingModule { }
