import { DetalleComponent } from './detalle/detalle.component';
import { RequerimientosComponent } from './requerimientos/requerimientos.component';
import { ReferenciasComponent } from './configuracion/referencias/referencias.component';
import { ProveedorComponent } from './configuracion/proveedor/proveedor.component';
import { ConfiguracionComponent } from './configuracion/configuracion.component';

import { HttpClientModule } from '@angular/common/http';
import { DemoMaterialModule } from '../../material-module';
// Angular
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { EstimadoComponent } from './estimado/estimado.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { AbastecimientoRoutingModule } from './abastecimiento-routing.module';
import { RouterModule } from '@angular/router';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AbastecimientoRoutingModule,
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    DemoMaterialModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule,
    TabsModule,
    DemoMaterialModule,
  ],
  declarations: [
    EstimadoComponent,
    ConfiguracionComponent,
    ProveedorComponent,
    ReferenciasComponent,
    RequerimientosComponent,
    DetalleComponent
  ]
})
export class AbastecimientoModule { }
