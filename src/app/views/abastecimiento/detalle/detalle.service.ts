import { Rutas } from './../../../rutas';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DetPed } from '../../../model/detPed';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DetalleService {

  constructor(private http: HttpClient, private r: Rutas) { }

  consultarDet(numero: number, tipo: string): Observable<DetPed[]> {
    return this.http.get<DetPed[]>(this.r['datosWMS'] + '/detPreOrden/' + numero + '/' + tipo);
  }

  modificarCantidad(numero: number, tipo: string, cnt: string, codigo: string) {
    return this.http.get(this.r['crearWMS'] + '/cambiarDetPreOrden/' + numero + '/' + tipo + '/' + cnt + '/' + codigo);
  }

  generarOrc(detalleOrc: string, numero: number, tipo: string): Observable<DetPed[]> {
    return this.http.get<DetPed[]>(this.r['crearCP'] + '/crearOrc/' + detalleOrc + '/' + numero + '/' + tipo);
  }

  rechazarOrc(numero: number, tipo: string) {
    return this.http.get(this.r['datosWMS'] + '/rechazarOrc/' + numero + '/' + tipo);
  }
}
