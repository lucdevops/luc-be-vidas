import { PreOrden } from './../../../model/preOrden';
import { Estimado } from './../../../model/estimado';
import { EstimadoService } from './../estimado/estimado.service';
import { Component, OnInit } from '@angular/core';
import { Marca } from '../../../model/marca';
import { Observable } from 'rxjs';
import { Referencias } from '../../../model/referencias';
import { flatMap, map } from 'rxjs/operators';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { RequerimientosService } from './requerimientos.service';
import { Ped } from '../../../model/ped';

@Component({
  templateUrl: './requerimientos.component.html'
})
export class RequerimientosComponent implements OnInit {

  est: PreOrden[];
  marca: Marca[];
  preOrc: Ped[];
  filteredRefe: Observable<Referencias[]>;
  controlRefe = new FormControl();
  formEst: FormGroup;
  datoMes;
  public loading: boolean;
  t;

  constructor(private fb: FormBuilder, private estimado: EstimadoService, private reque: RequerimientosService) { }

  ngOnInit() {

    this.formEst = this.fb.group({
      tipo: [''],
      mar: [''],
    });

    this.estimado.cargarMar('').subscribe(mar => {
      this.marca = mar;
    });

    this.reque.consultarPreOrc().subscribe(ped => {
      this.preOrc = ped;
    });

    this.filteredRefe = this.controlRefe.valueChanges
      .pipe(
        map(value => typeof value === 'string' ? value : value),
        flatMap(value => value ? this.filterRefe(value) : [])
      );

  }

  filterRefe(value2: string): Observable<Referencias[]> {
    const filterValue2 = value2.toLowerCase();
    return this.estimado.cargaRef(filterValue2);
  }

  consultar(formValue: any) {

    this.reque.consultar(this.formEst.value).subscribe(est => {
      this.est = est;
      location.reload();
    });
  }

}
