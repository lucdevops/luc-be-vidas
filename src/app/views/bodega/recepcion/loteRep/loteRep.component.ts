import { Um } from './../../../../model/um';
import { LoteRepService } from './loteRep.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Medida } from '../../../../model/medida';
import { Validators, FormGroup, FormBuilder, FormArray } from '@angular/forms';

@Component({
  templateUrl: './loteRep.component.html',
})
export class LoteRepComponent implements OnInit {

  codigo: string;
  cantidad: number;
  unidades: Um[];
  medida: Medida = new Medida;
  miFormulario: FormGroup;
  pointIndex = 0;
  focus: any;
  divcj: number;
  divdp: number;
  numcajas: number;
  numdisplay: number;
  numunidades: number;
  numero;
  tipo;

  public isShowingSecond: boolean;

  constructor(private rutaActiva: ActivatedRoute, private fb: FormBuilder, private loteRepService: LoteRepService) { }

  ngOnInit() {
    this.numero = this.rutaActiva.snapshot.params.numero;
    this.tipo = this.rutaActiva.snapshot.params.tipo;
    this.codigo = this.rutaActiva.snapshot.params.codigo;
    this.cantidad = this.rutaActiva.snapshot.params.cantidad;

    this.miFormulario = this.fb.group({
      items: this.fb.array([this.fb.group({
        lote: ['', [Validators.required]],
        fechaVen: ['', [Validators.required]],
        cj: ['0'],
        dp: ['0'],
        und: ['0']
      })])
    });

    this.loteRepService.consultarUm(this.codigo).subscribe(r => {
      this.unidades = r;
      for (let i = 0; i < this.unidades.length; i++) {
        this.medida.codigo = this.unidades[i].codigo;
        if (this.medida === null) {
          if (this.unidades[i].org === 'CJ') {
            this.medida.cajas = this.unidades[i].conv;
          } else {
            this.medida.display = this.unidades[i].conv;
          }
        } else {
          if (this.unidades[i].org === 'CJ') {
            this.medida.cajas = this.unidades[i].conv;
          } else {
            this.medida.display = this.unidades[i].conv;
          }
        }
      }

      if (this.medida.cajas > 0) {
        this.divcj = this.cantidad / this.medida.cajas;
        const divide = this.divcj.toString().split('.');

        this.numcajas = +divide[0];

        if (divide.length === 0) {
          this.numdisplay = 0;
          this.numunidades = 0;
        } else {
          if (this.medida.display > 0) {
            this.divdp = (this.medida.cajas * (this.divcj - (+divide[0]))) / this.medida.display;
            const dividedp = this.divdp.toString().split('.');
            this.numdisplay = +dividedp[0];
            if (dividedp.length === 0) {
              this.numunidades = 0;
            } else {
              this.numunidades = Math.round((this.divdp - (+dividedp[0])) * this.medida.display);
            }
          } else {
            this.numdisplay = 0;
            this.numunidades = Math.round(this.medida.cajas * (this.divcj - (+divide[0])));
          }
        }
      } else {
        this.numcajas = 0;
        this.numdisplay = 0;
        this.numunidades = this.cantidad;
      }
    });
  }

  get getItems() {
    return this.miFormulario.get('items') as FormArray;
  }

  addItem() {
    const control = <FormArray>this.miFormulario.controls['items'];
    control.push(this.fb.group({ lote: ['', Validators.required], fechaVen: ['', Validators.required], cj: [0], dp: [0], und: [0] }));
    this.pointIndex++;
  }

  removeItem(index: number) {
    const control = <FormArray>this.miFormulario.controls['items'];
    control.removeAt(index);
    this.pointIndex--;
  }

  public setFocus(fieldToFocus: number): void {
    this.focus = fieldToFocus;
  }

  public toggleSecond(fieldToFocus: number): void {
    this.isShowingSecond = !this.isShowingSecond;
    this.setFocus(fieldToFocus);
  }

  onInputEntry(event, nextInput) {

    let input = event.target;
    let length = input.value.length;
    let maxLength = input.attributes.maxlength.value;

    if (length >= maxLength) {
      nextInput.focus();
    }
  }

  guardarLote(form: any) {

    let t = 0;

    for (let i = 0; i < form.items.length; i++) {
      let total = 0;
      if (this.medida.cajas !== undefined) {
        total = total + (this.medida.cajas * form.items[i].cj);
      }
      if (this.medida.display !== undefined) {
        total = total + (this.medida.display * form.items[i].dp);
      }
      total = total + (+form.items[i].und);
      t = t + total;
    }

    if (t > this.cantidad) {
      alert('LA CANTIDAD INGRESADA NO PUEDE SUPERAR LA CATIDAD ORDENADA');
    } else {
      for (let i = 0; i < form.items.length; i++) {
        let total = 0;
        if (this.medida.cajas !== undefined) {
          total = total + (this.medida.cajas * form.items[i].cj);
        }
        if (this.medida.display !== undefined) {
          total = total + (this.medida.display * form.items[i].dp);
        }
        total = total + (+form.items[i].und);
        // tslint:disable-next-line: max-line-length
        this.loteRepService.guardarLoteOrc(this.tipo, this.numero, this.codigo, total, form.items[i].lote, form.items[i].fechaVen, t).subscribe();
      }
      //console.log(errores);
      window.location.href = '#/bodega/detalleRep/' + this.numero + '/' + this.tipo;
    }

  }

}
