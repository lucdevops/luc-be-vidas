import { Rutas } from './../../../../rutas';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { DetPed } from '../../../../model/detPed';

@Injectable({
  providedIn: 'root'
})
export class DetalleRepService {

  constructor(private http: HttpClient, private r: Rutas) { }

  consultarDet(numero: number, tipo: string): Observable<DetPed[]> {
    return this.http.get<DetPed[]>(this.r['datosWMS'] + '/detPreOrden/' + numero + '/' + tipo);
  }

  filter(id: number, det: DetPed[]): DetPed[] {
    let dets = det.filter(item => item.refe.codigo === id);
    return dets;
  }

  noLlego(codigo, numero, tipo) {
    return this.http.get(this.r['crearWMS'] + '/noLlego/' + numero + '/' + tipo + '/' + codigo);
  }

  finalizarOrden(numero, tipo) {
    return this.http.get(this.r['crearCP'] + '/finalizarOrden/' + numero + '/' + tipo);
  }

}
