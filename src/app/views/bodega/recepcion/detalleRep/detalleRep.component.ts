import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DetalleRepService } from './detalleRep.service';
import { DetPed } from '../../../../model/detPed';
import { Observable } from 'rxjs';

@Component({
  templateUrl: './detalleRep.component.html',
})
export class DetalleRepComponent implements OnInit {

  numero: number;
  tipo: string;
  det: DetPed[];
  public loading = true;
  t;

  constructor(private rutaActiva: ActivatedRoute, private detRepService: DetalleRepService) { }

  ngOnInit() {
    this.numero = this.rutaActiva.snapshot.params.numero;
    this.tipo = this.rutaActiva.snapshot.params.tipo;

    this.detRepService.consultarDet(this.numero, this.tipo).subscribe(r => {
      this.det = r;
    });

  }

  filterCom(id: number) {
    this.det = this.detRepService.filter(id, this.det);

    if (this.det.length !== 0) {
      // tslint:disable-next-line: max-line-length
      window.location.href = '#/bodega/loteRep/' + this.det[0].refe.codigo + '/' + this.det[0].cantidad + '/' + this.tipo + '/' + this.numero;
    } else {
      console.log('PRODUCTO NO EXISTE');
    }
  }

  getColor(cnt, cntRec) {

    if (cntRec === null) {
      return 'white';
    } else if (cnt === cntRec) {
      return 'lightgreen';
    } else if (cnt !== cntRec) {
      if (cntRec === 0) {
        return 'red';
      } else {
        return '#669999';
      }
    }
  }

  noLlego(codigo) {
    this.detRepService.noLlego(codigo, this.numero, this.tipo).subscribe();

    for (let i = 0; i < this.det.length; i++) {
      if (this.det[i].refe.codigo === codigo) {
        this.det[i].cantidadRec = 0;
      }

    }
  }

  finalizarOrden() {
    let validado = 0, faltante = 0;

    for (let i = 0; i < this.det.length; i++) {
      if (this.det[i].estado === 'V') {
        validado++;
      } else {
        faltante++;
      }
    }

    console.log(faltante);

    if (faltante !== 0) {
      alert('FALTAN PRODUCTOS POR RECIBIR');
    } else if (validado === this.det.length) {
      this.detRepService.finalizarOrden(this.numero, this.tipo).subscribe(resul => {
        if (resul === true) {
          this.t = true;
          this.loading = false;
        }
      });
    }
  }

  atras() {
    window.location.href = '#/bodega/recepcion';
  }
}
