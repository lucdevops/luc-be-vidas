import { Rutas } from './../../../rutas';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Cliente } from '../../../model/clientes';

@Injectable({
  providedIn: 'root'
})
export class AgendadosService {

  constructor(private http: HttpClient, private r: Rutas) { }

  cargarCliDiaAgen(): Observable<Cliente[]> {
    return this.http.get<Cliente[]>(this.r['datosWMS'] + '/cargaCliDiaAgen/VITAAVE');
  }

  cargaCli(descripcion): Observable<Cliente[]> {
    return this.http.get<Cliente[]>(this.r['datosWMS'] + '/cargaCli/' + descripcion);
  }

}
