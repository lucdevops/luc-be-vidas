import { Injectable } from '@angular/core';
import  {Observable} from 'rxjs';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import { Loggin } from '../../model/loggin';

@Injectable({
    providedIn: 'root'
  })
  export class LogginService {
  
 private urlenpoint :string = "http://192.168.1.250:8085/api/loggin";
  constructor(private http:HttpClient) { }


singin(user,password):Observable<Loggin []>
{

 var url= this.urlenpoint+"/"+user+"/"+password
  return this.http.get<Loggin[]>(url)
}

getuser():boolean
{
    let user = sessionStorage.getItem('usuario')
    if(user===null || user==="")
    { 
    return false
    }
    else
    {
      console.log(user)
      return true
    }
    return false

  }

logout()
  {
    sessionStorage.clear();
}









}