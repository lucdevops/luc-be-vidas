import { Component, OnInit } from '@angular/core';
import { Loggin } from '../../model/loggin';
import { LogginService } from '../login/loggin.service';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';

@Component({
 // selector: 'app-loggin',
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {
  user:string=""
  password:string=""
  Loggin:Loggin[]
  isloggin:boolean=false
  
  
  constructor(public LogginService:LogginService) { }
  
    ngOnInit() {
  
      this.isloggin=this.LogginService.getuser()
  
  
    }
  
  onclicksingin()
  {
  
  this.LogginService.singin(this.user,this.password).subscribe(
    loggin=>{this.Loggin=loggin
  
      if(this.Loggin.length >0)
      {
        sessionStorage.setItem("usuario",this.Loggin[0].usuario)
        sessionStorage.setItem("nombre",this.Loggin[0].nombre)
        sessionStorage.setItem("cargo",this.Loggin[0].cargo)
        var x = sessionStorage.getItem("usuario");
        console.log(x)
        window.location.href = '/home';
      }
      else{
          alert("El usuario o la contraseña que ingresa son incorrectos");  
      }
     }
  );
  
  }
  
  }