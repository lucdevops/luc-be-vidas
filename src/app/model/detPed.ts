import { Referencias } from './referencias';

export class DetPed {
  id: number;
  sw: number;
  bodega: number;
  numero: number;
  refe: Referencias;
  seq: number;
  cantidad: number;
  cantidadDes: number;
  cantidadRec: number;
  valorUni: number;
  porcentajeIva: number;
  porcentajeDes: number;
  und: string;
  tipo: string;
  estado: string;
  cantidadUnd: number;
  stock: number;
  invSeg: number;
  diasInv: number;
}
