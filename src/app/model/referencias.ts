export class Referencias {
  descripcion: string;
  codigo: number;
  generico: string;
  clase: string;
  contable: number;
  grupo: string;
  subgrupo: string;
  nit: number;
  marca: string;
  valorUni: number;
  iva: number;
  inventario: number;
  alto: number;
  ancho: number;
  cajas: number;
  display: number;
}
