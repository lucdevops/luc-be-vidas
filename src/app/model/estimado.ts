import { EstimadoKey } from './estimadoKey';


export class Estimado {

  llave: EstimadoKey;
  estimado: number;
  cantidad: number;
  cantidadDia: number;
  precio: number;

}
