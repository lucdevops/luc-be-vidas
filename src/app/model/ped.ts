import { PedKey } from './pedKey';
import { Cliente } from './clientes';

export class Ped {

  pedKey: PedKey;
  tercero: Cliente;
  anulado: number;
  soId: string;
  /*Date fecha: date;
  Date fechaHora;
  Date fechaHoraEnt;*/
  fecha: Date;
  usuario: string;
  pc: string;
  nitDestino: number;
  vendedor: Cliente;
  condicion: string;
  codigoDir: number;
  codigoDirDest: number;
  notas: string;
  estado: string;
  proveedor: Cliente;
  tipo: string;
  despacho: string;
}
