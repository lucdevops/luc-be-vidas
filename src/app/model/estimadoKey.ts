import { Referencias } from './referencias';
import { Cliente } from './clientes';

export class EstimadoKey {

  ano: number;
  mes: number;
  tipo: string;
  cliente: Cliente;
  referencia: Referencias;

}
